package com.imcs.afp2pdf;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Element;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.RomanList;
import com.itextpdf.text.factories.RomanNumberFactory;

public class RomanListAdapted extends RomanList{
	
	public RomanListAdapted(boolean lowercase, int symbolIndent) {
		super(true, symbolIndent);
		this.lowercase = lowercase;
	}
	
	public boolean add(Element o) {
		if (o instanceof ListItem) {
			ListItem item = (ListItem) o;
			Chunk chunk;
			chunk = new Chunk(preSymbol, symbol.getFont());
            chunk.setAttributes(symbol.getAttributes());
			chunk.append(RomanNumberFactory.getString(first + list.size(), lowercase));
			chunk.append(")");
			item.setListSymbol(chunk);
			item.setIndentationLeft(symbolIndent, autoindent);
			item.setIndentationRight(0);
			list.add(item);
		} else if (o instanceof List) {
			List nested = (List) o;
			nested.setIndentationLeft(nested.getIndentationLeft() + symbolIndent);
			first--;
			return list.add(nested);
		}
		return false;
		}

}
