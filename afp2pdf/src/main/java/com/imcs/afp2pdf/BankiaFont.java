package com.imcs.afp2pdf;

import java.io.IOException;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;

public class BankiaFont {

	static BaseFont getBaseFont() throws DocumentException, IOException {
		return BaseFont.createFont(
				"src/main/resources/bankia-regular-webfont.ttf",
				BaseFont.WINANSI, BaseFont.EMBEDDED);
				
	}

	static BaseFont getBaseBoldFont() throws DocumentException, IOException {
		return BaseFont.createFont(
				"src/main/resources/bankia-bold-webfont.ttf",
				BaseFont.WINANSI, BaseFont.EMBEDDED);
	}

}
