package com.imcs.afp2pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.IOException;
import java.net.MalformedURLException;

public class HeaderFooterPageEvent extends PdfPageEventHelper {

	private PdfTemplate t;
	private Image total;

	public void onOpenDocument(PdfWriter writer, Document document) {
		t = writer.getDirectContent().createTemplate(30, 16);
		try {
			total = Image.getInstance(t);
			total.setRole(PdfName.ARTIFACT);
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	@Override
	public void onEndPage(PdfWriter writer, Document document) {
		try {
			addHeader(writer);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			addFooter(writer);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addHeader(PdfWriter writer) throws MalformedURLException,
			IOException {
		PdfPTable header = new PdfPTable(2);
		try {
			// set defaults
			header.setWidths(new int[] { 100, 666 });
			header.setTotalWidth(765);
			header.setLockedWidth(true);
			// header.getDefaultCell().setFixedHeight(105);
			// header.getDefaultCell().setBorder(Rectangle.BOX);
			// header.getDefaultCell().setBorderColor(BaseColor.GREEN);
			// add image
			Image logo = Image.getInstance(HeaderFooterPageEvent.class
					.getClassLoader().getResource("bankia.png"));
			PdfPCell image = new PdfPCell(logo, true);
			image.setBorder(0);
			header.addCell(image);
			// add text
			PdfPCell text = new PdfPCell();
			text.setBorder(0);
			text.setPaddingBottom(15);
			text.setPaddingLeft(10);
			// text.setBorder(Rectangle.BOX);
			// text.setBorderColor(BaseColor.RED);
			Paragraph paragraph = new Paragraph();
			// Font font =
			// Font.getInstance(HeaderFooterPageEvent.class.getClassLoader().getResource("bankia.png"));
			// FontFactory.register(path)
			paragraph.add(new Phrase("INFORMACIÓN FINANCIERA-PYME", new Font(
					BankiaFont.getBaseBoldFont(), 14)));
			// text.addElement(new Phrase("otra phrase", new
			// Font(Font.FontFamily.HELVETICA, 8)));
			paragraph.setAlignment(Element.ALIGN_RIGHT);
			text.addElement(paragraph);
			header.addCell(text);
			// header.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			// header.addCell(new
			// Phrase("Inscrita en Registro Mercantil de Valencia, Tomo 9.341, Libro 6.623. Folio 104, Inscripción 183, Sección General, Hoja V-17.274. CIF: A-14010342",
			// new Font(Font.FontFamily. HELVETICA, 5, Font.NORMAL)));
			// add text2
			// PdfPCell text2 = new PdfPCell();
			// text.setPaddingBottom(15);
			// text.setPaddingLeft(10);
			// text.setBorder(Rectangle.BOX);
			// text.setBorderColor(BaseColor.RED);
			// text.addElement(new Phrase("INFORMACIÓN FINANCIERA-PYME", new
			// Font(Font.FontFamily.HELVETICA, 14)));
			// text.addElement(new Phrase("otra phrase", new
			// Font(Font.FontFamily.HELVETICA, 8)));
			// header.addCell(text2);
			// write content
			header.writeSelectedRows(0, -1, 42.5f, 570,
					writer.getDirectContent());
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	private void addFooter(PdfWriter writer) throws MalformedURLException,
			IOException {
		PdfPTable footer = new PdfPTable(4);
		footer.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
		try {
			// set defaults
			footer.setWidths(new float[] { 0.30f, 1.3f, 15f, 1.6f });
			footer.setTotalWidth(765);
			footer.setLockedWidth(true);
			footer.getDefaultCell().setFixedHeight(40);
			// footer.getDefaultCell().setBorder(Rectangle.TOP);
			// footer.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
			// add current page count
			footer.addCell(new Phrase(String.format("%d  /",
					writer.getPageNumber()), new Font(BankiaFont.getBaseFont(),
					6)));
			// add placeholder for total page count
			PdfPCell totalPageCount = new PdfPCell(total);
			totalPageCount.setBorder(0);
			// totalPageCount.setBorder(Rectangle.TOP);
			// totalPageCount.setBorderColor(BaseColor.RED);
			footer.addCell(totalPageCount);
			// add legal company data
			// footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell legal = new PdfPCell();
			legal.setBorder(0);
			legal.setPaddingTop(8);
			Paragraph paragraph4 = new Paragraph();
			paragraph4
					.add(new Phrase(
							"Inscrita en Registro Mercantil de Valencia, Tomo 9.341, Libro 6.623. Folio 104, Inscripción 183, Sección General, Hoja V-17.274. CIF: A-14010342",
							new Font(BankiaFont.getBaseFont(), 5, Font.NORMAL)));
			paragraph4.setAlignment(Element.ALIGN_CENTER);
			legal.addElement(paragraph4);
			footer.addCell(legal);
			// write web and phone
			PdfPCell webAndPhone = new PdfPCell();
			webAndPhone.setBorder(0);
			Paragraph paragraph1 = new Paragraph(), paragraph2 = new Paragraph();
			paragraph1.add(new Phrase("bankia.es", new Font(BankiaFont
					.getBaseFont(), 8)));
			paragraph2.add(new Phrase("902 24 68 10", new Font(BankiaFont
					.getBaseFont(), 8)));
			paragraph1.setAlignment(Element.ALIGN_RIGHT);
			paragraph2.setAlignment(Element.ALIGN_RIGHT);
			paragraph1.setSpacingAfter(-3);
			paragraph2.setSpacingBefore(-3);
			webAndPhone.addElement(paragraph1);
			webAndPhone.addElement(paragraph2);
			// text.addElement(new Phrase("otra phrase", new
			// Font(Font.FontFamily.HELVETICA, 8)));
			footer.addCell(webAndPhone);
			// write page
			PdfContentByte canvas = writer.getDirectContent();
			canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
			footer.writeSelectedRows(0, -1, 34, 50, canvas);
			canvas.endMarkedContentSequence();
		} catch (DocumentException de) {
			throw new ExceptionConverter(de);
		}
	}

	public void onCloseDocument(PdfWriter writer, Document document) {
		Font font = null;
		try {
			font = new Font(BankiaFont.getBaseFont(), 6);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int totalLength = String.valueOf(writer.getPageNumber()).length();
		int totalWidth = totalLength * 5;
		ColumnText.showTextAligned(t, Element.ALIGN_RIGHT,
				new Phrase(String.valueOf(writer.getPageNumber()), font),
				totalWidth, 8, 0);
	}

}
