package com.imcs.afp2pdf;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class CreatePdf {
	
    static String[] mmWidths;
    static String[] headers;   

	/**
	 * @param args
	 * @throws DocumentException
	 * @throws IOException
	 */
	/**
	 * @param args
	 * @throws DocumentException
	 * @throws IOException
	 */
	/**
	 * @param args
	 * @throws DocumentException
	 * @throws IOException
	 */
	/**
	 * @param args
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void main(String... args) throws DocumentException, IOException {		
    	// delete file previously created in any other test, and print in console true if file is deleted
    	File file = new File("InformeFinancieroPyme.pdf");
    	file.delete();    	
    	// load properties
    	Properties properties = new Properties();
        InputStream in = CreatePdf.class.getClassLoader().getResourceAsStream("app.properties");
        properties.load(in);
        in.close();
        // set fonts with red color font in several sizes
    	Font fontRed9 = new Font(BankiaFont.getBaseFont(), 9);
    	fontRed9.setColor(BaseColor.RED);
    	Font fontRed10 = new Font(BankiaFont.getBaseFont(), 10);
    	fontRed10.setColor(BaseColor.RED);
    	Font fontRed11 = new Font(BankiaFont.getBaseFont(), 11);
    	fontRed11.setColor(BaseColor.RED);
    	// create iText document and a new pdf file
    	Document document = new Document(PageSize.A4.rotate(), 42.5f, 28.3f, 70f, 42.5f);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("InformeFinancieroPyme.pdf"));
        // add header and footer
        HeaderFooterPageEvent event = new HeaderFooterPageEvent();
        writer.setPageEvent(event);
        // open document to write on it 
        document.open();        
        // 1ST PAGE
		document.add(new Paragraph(properties.getProperty("index")));
		document.add(Chunk.NEWLINE);
        // INDEX
		PdfPCell cell;
        List list = new List(List.ORDERED, 17);
        list.setIndentationLeft(18);
        List sublist;
        String[] dataProps = {"index.identidad", "index.declaraciones", "index.datos", "index.historial", "index.extracto", "index.riesgo"};
        String[] data = new String[7];
        for(int i = 0; i < dataProps.length; i++) {
        	data[i] = properties.getProperty(dataProps[i]);
        }
        dataProps = new String[]{"index.historial.creditos", "index.historial.obligaciones", "index.historial.concursos", "index.historial.seguros"};
        String[] data2 = new String[4];
        for(int i = 0; i < dataProps.length; i++) {
        	data2[i] = properties.getProperty(dataProps[i]);
        }
        dataProps = new String[]{"index.riesgo.calificacion", "index.riesgo.posicion"};
        String[] data3 = new String[2];
        for(int i = 0; i < dataProps.length; i++) {
        	data3[i] = properties.getProperty(dataProps[i]);
        }  
        for(int i = 0; i < data.length; i++) {
            ListItem item = new ListItem(data[i], new Font(BankiaFont.getBaseFont(), 10));
            if (i==4) {            	
            	sublist = new RomanListAdapted(List.ALPHABETICAL, 17);
            	sublist.setIndentationLeft(22);
                for(int j = 0; j < data2.length; j++) {
                	ListItem item2 = new ListItem(data2[j], new Font(BankiaFont.getBaseFont(), 10));
                	item2.setAlignment(Element.ALIGN_LEFT);
                	item2.setSpacingAfter(10);
                    sublist.add(item2);
                }
                item.setAlignment(Element.ALIGN_LEFT);
                list.add(sublist);                
            }
            if (i==6) {            	
            	sublist = new ListAdapted(List.ORDERED, 22);
            	sublist.setIndentationLeft(10);
                for(int j = 0; j < data3.length; j++) {
                	ListItem item2 = new ListItem(data3[j], new Font(BankiaFont.getBaseFont(), 10));
                	item2.setAlignment(Element.ALIGN_LEFT);
                	item2.setSpacingAfter(10);
                    sublist.add(item2);
                }
                item.setAlignment(Element.ALIGN_LEFT);
                list.add(sublist);                
            }
            item.setAlignment(Element.ALIGN_LEFT);
            item.setSpacingAfter(10);
            list.add(item);
        }
        document.add(list);
        // 2ND PAGE
        document.newPage();
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("1.   Datos identificativos", new Font(BankiaFont.getBaseBoldFont(), 11)));
        PdfPTable identification = new PdfPTable(2);        
        identification.setWidths(new float[]{14.2f,60});
        identification.setTotalWidth(710);
        identification.setLockedWidth(true);
        identification.getDefaultCell().setFixedHeight(20.41f);
        identification.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        dataProps = new String[] {"datos.nombre", "datos.bankia", "datos.acreditado", "datos.XXX", "datos.nif", "datos.XX", "datos.fechadoc", "datos.X"};
        data = new String[8];
        for(int i = 0; i < dataProps.length; i++) {
        	if ((i % 2 != 0 && i > 1)) {
        		identification.getDefaultCell().setBorder(PdfPCell.BOTTOM);
        	 }
        	identification.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        	if (i % 2 != 0) {
        		identification.getDefaultCell().setBorder(PdfPCell.TOP | PdfPCell.BOTTOM);
        	}
        	data[i] = properties.getProperty(dataProps[i]);
        	if (i % 2 != 0 && i > 1) {
        		identification.addCell(new Phrase(data[i], fontRed9));
        	}
        	else identification.addCell(new Phrase(data[i], new Font(BankiaFont.getBaseFont(), 10)));
        }        
        try {
            identification.setSpacingBefore(30f);
            identification.setSpacingAfter(20f);
            document.add(identification);
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
        document.add(new Paragraph(properties.getProperty("datos.observacion1"), new Font(BankiaFont.getBaseFont(), 10)));
        document.add(new Paragraph(properties.getProperty("datos.observacion2"), new Font(BankiaFont.getBaseFont(), 10)));
        // 3RD PAGE
        document.newPage();
        document.add(new Paragraph("2.   Declaraciones a la Central de Información de Riesgos del Banco de España", new Font(BankiaFont.getBaseBoldFont(), 11)));
        float[] columnWidths = {1.31f, 1.8f, 1.3f, 1.35f, 1.2f, 1.2f, 0.8f, 1.45f, 0.8f, 1.45f, 1.5f, 1.3f, 3f, 3f, 3f, 3f };
        PdfPTable declarations = new PdfPTable(columnWidths);
        declarations.setTotalWidth(786.6F);
        declarations.setLockedWidth(true);
        try {        	
        	declarations.setHeaderRows(2); 
        	// 1st row
        	declarations.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        	declarations.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
        	declarations.getDefaultCell().setFixedHeight(31);
            String[] declarationsData = {"Proceso", "Intervención (*)", "Solidario o colectivo", "Tipo de producto (*)", "Moneda", "Plazo residual (*)"};        
            for(int i = 0; i < declarationsData.length; i++) {
                cell = new PdfPCell(new Phrase(declarationsData[i], new Font(BankiaFont.getBaseFont(), 9)));
                cell.setRowspan(2);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBackgroundColor(BaseColor.GRAY);
                declarations.addCell(cell);
            }
            String[] declarationsData2 = {"Garantía real principal", "Garantía personal principal"};        
            for(int i = 0; i < declarationsData2.length; i++) {
                cell = new PdfPCell(new Phrase(declarationsData2[i], new Font(BankiaFont.getBaseFont(), 9)));
                cell.setColspan(2);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBackgroundColor(BaseColor.GRAY);
                declarations.addCell(cell);
            }
            String[] declarationsData3 = {"Situación de la operación (*)", "Directo o Indirecto"};        
            for(int i = 0; i < declarationsData3.length; i++) {
                cell = new PdfPCell(new Phrase(declarationsData3[i], new Font(BankiaFont.getBaseFont(), 9)));
                cell.setRowspan(2);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBackgroundColor(BaseColor.GRAY);
                declarations.addCell(cell);
            }            
            cell = new PdfPCell(new Phrase("Riesgo dispuesto", new Font(BankiaFont.getBaseFont(), 9)));
            cell.setColspan(3);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(BaseColor.GRAY);
            declarations.addCell(cell);            
            cell = new PdfPCell(new Phrase("Riesgo disponible", new Font(BankiaFont.getBaseFont(), 9)));
            cell.setRowspan(2);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBackgroundColor(BaseColor.GRAY);
        	declarations.addCell(cell);
        	// 2nd row 
        	declarations.getDefaultCell().setBackgroundColor(BaseColor.GRAY);
            String[] declarationsData4 = {"Tipo (*)", "Cobertura", "Tipo (*)", "Cobertura", "Total", "Importes vencidos", "Intereses de demora y gastos exigibles"};        
            for(int i = 0; i < declarationsData4.length; i++) {
            	declarations.addCell(new Phrase(declarationsData4[i], new Font(BankiaFont.getBaseFont(), 9)));
            }
            // 3rd and following rows;
        	for (int i = 0; i<11; i++) {
        		for (int j = 0; j < 11; j++) {
                	String[] auxArray = new String[]{"XXXXXX", "XXX", "X", "XXX", "XXX", "XXX", "XXX", "XXXXXXX", "XXX", "XXXXXXX", "XXX"};
                	cell = new PdfPCell(new Phrase(auxArray[j], fontRed9));
	                cell.setRowspan(2);
	                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	                declarations.addCell(cell);
                }
                declarations.getDefaultCell().setFixedHeight(15);
                declarations.getDefaultCell().setBackgroundColor(BaseColor.WHITE);
                declarations.addCell(new Phrase("X", fontRed9));
                for (int j = 0; j <9; j++) {
                	if (j == 4) {
                		declarations.addCell(new Phrase("X", fontRed9));
                		continue;
                	}
                	declarations.addCell(new Phrase("Z.ZZZ.ZZZ.ZZZ.ZZZ.ZZZ", fontRed9));
				}

            }
            // set 'row position' of the row that must be married to its precedent row 
            declarations.keepRowsTogether(1552);
        	// add table to the page with the chosen space before and after it, and set a possible break point also
            declarations.setSpacingBefore(15f);
            declarations.setSpacingAfter(13f);
            if (declarations.getRow(19) != null) {
            	declarations.setBreakPoints(19);
            } 
            document.add(declarations);
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
        document.add(new Paragraph("(*) Lista de valores en Anejo", new Font(BankiaFont.getBaseFont(), 11)));
        document.add(new Paragraph("Los importes figuran en euros.", new Font(BankiaFont.getBaseFont(), 11)));
        // 4TH PAGE
        document.newPage();
        // RaiInformation table
        document.add(new Paragraph(properties.getProperty("comunicaciones"), new Font(BankiaFont.getBaseBoldFont(), 11)));
        document.add(Chunk.NEWLINE);
        Paragraph paragraph = new Paragraph(properties.getProperty("comunicaciones.rai"), new Font(BankiaFont.getBaseBoldFont(), 11));
        Chunk chunk = new Chunk(" XXXXXXXX", fontRed11);
        paragraph.add(chunk);
        document.add(paragraph);
        // build a table with the given mmWidths and headers
        mmWidths =  new String[] {"33,5", "70", "34", "20", "13,5", "31,5", "31.5"};
        headers =  new String[] {"comunicaciones.rai.efecto","comunicaciones.rai.situacion", "comunicaciones.rai.tipo", "comunicaciones.rai.vencimiento", "comunicaciones.rai.moneda", "comunicaciones.rai.impagado", "comunicaciones.rai.nominal"};        
        PdfPTable RaiInformation = TableBuilder.getTable(mmWidths, headers, 10);
        try {           
            // 1st row
            // 2nd and following rows
            for (int i = 0; i<2; i++) {
                for (int j = 0; j < 7; j++) {
                	// auxiliary array for simulated data
                	String[] auxArray = new String[]{"99999999999999999", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXX", "XXXXXXXX", "XXX", "Z.ZZZ.ZZZ.ZZZ.ZZZ,ZZ", "Z.ZZZ.ZZZ.ZZZ.ZZZ,ZZ"};
                	cell = new PdfPCell(new Phrase(auxArray[j], fontRed9));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    RaiInformation.addCell(cell);
                }
            }
            // set 'row position' of the row that will be married to the precedent row 
            RaiInformation.keepRowsTogether(1552);
        	// add table to the page with chosen space before and after it, and set a possible break point
            RaiInformation.setSpacingBefore(25f);
            RaiInformation.setSpacingAfter(8f);
            if (RaiInformation.getRows().size() > 18) {
                RaiInformation.setBreakPoints(19);
            }  
            document.add(RaiInformation);
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
        document.add(new Paragraph(properties.getProperty("comunicaciones.rai.observacion"), new Font(BankiaFont.getBaseFont(), 10)));
        document.add(Chunk.NEWLINE);
        document.add(Chunk.NEWLINE);
        paragraph = new Paragraph(properties.getProperty("comunicaciones.mora"), new Font(BankiaFont.getBaseBoldFont(), 11));
        chunk = new Chunk(" XXXXXXXX", fontRed11);
        paragraph.add(chunk);
        document.add(paragraph);
        // DelayInformation table
        // build a table with the given mmWidths and headers
        mmWidths =  new String[] {"40,5", "19", "19,5", "16", "25", "27", "22", "26", "31,5", "19,5"};
        headers =  new String[] {"comunicaciones.mora.referencia", "comunicaciones.mora.fecha", "comunicaciones.mora.vencimiento", "comunicaciones.mora.producto", "comunicaciones.mora.situacion", "comunicaciones.mora.recibos", "comunicaciones.mora.impagado", "comunicaciones.mora.cuota", "comunicaciones.mora.importe", "comunicaciones.mora.intervencion"};
        PdfPTable DelayInformation = TableBuilder.getTable(mmWidths, headers, 10);
        try {           
            // 1st row
        	// 2nd and following rows        	       	
            for (int i = 0; i<2; i++) {
                for (int j = 0; j < 10; j++) {
                	// auxiliary array for simulated data
                	String[] auxArray = new String[]{"99999999999999999", "XXXXXXXX", "XXXXXXXX", "XX", "XXXXXXXXXXX", "Z.ZZZ", "XXXXXXXX", "XXXXXXXX", "Z.ZZZ.ZZZ.ZZZ.ZZZ,ZZ", "XXXXXXX"};
                	cell = new PdfPCell(new Phrase(auxArray[j], fontRed9));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    DelayInformation.addCell(cell);
                }
            }
            // set 'row position' of the row that will be married to the precedent row 
            DelayInformation.keepRowsTogether(1552);
        	// add table to the page with chosen space before and after it, and set a possible break point
            DelayInformation.setSpacingBefore(25f);
            DelayInformation.setSpacingAfter(8f);
            if (DelayInformation.getRows().size() > 18) {
                DelayInformation.setBreakPoints(19);
            }
            // set 'row position' of the row that will be married to the precedent row 
            DelayInformation.keepRowsTogether(1552);
            document.add(DelayInformation);
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
        document.add(new Paragraph(properties.getProperty("comunicaciones.mora.observacion1"), new Font(BankiaFont.getBaseFont(), 10)));
        document.add(new Paragraph(properties.getProperty("comunicaciones.mora.observacion2"), new Font(BankiaFont.getBaseFont(), 10)));
        // 5TH PAGE
        document.newPage();
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("4.  Historial crediticio.", new Font(BankiaFont.getBaseBoldFont(), 11)));
        document.add(Chunk.NEWLINE);
        document.add(new Paragraph("4.i) Relación de créditos históricos y vigentes, y de los importes pendientes de amortización.", new Font(BankiaFont.getBaseBoldFont(), 11)));
        document.add(Chunk.NEWLINE);
        paragraph = new Paragraph("Para el formato y contenido de los campos indicados en las tablas siguientes, se considerarán las instrucciones y las definiciones establecidas en la norma 6 de la Circular 6/2016, de 30 de junio, del Banco de España, así como en los módulos B.2 y C.1, del anejo 2, «Instrucciones para elaborar los módulos de datos», de la Circular 1/2013, de 24 de mayo, del Banco de España, sobre la Central de Información de Riesgos. Adicionalmente, deberán considerarse las aclaraciones realizadas a continuación:", new Font(BankiaFont.getBaseFont(), 10));
        paragraph.setSpacingBefore(10);
        paragraph.setLeading(10, 0);
        document.add(paragraph);
        paragraph = new Paragraph("(1) En este cuadro se recogen también aquellas operaciones que, conforme a la normativa contable aplicable a las transferencias de activos, se han dado de baja, total o parcialmente, del activo de la entidad por haberse cedido el riesgo a un tercero, pero la entidad conserva la gestión de esas operaciones frente a sus titulares.\n(2) Se incluirán todos aquellos productos de activo vigentes a la fecha de elaboración del documento «Información Financiera-PYME», así como aquellos productos de activo cancelados durante los cinco años anteriores a la fecha de la notificación o a la fecha de solicitud, tal y como se especifica en la norma 6 de la Circular 6/2016, de 30 de junio, del Banco de España. El código de la operación podrá ser aquel con el que se identifica la operación en el módulo B.2, del anejo 2, «Instrucciones para elaborar los módulos de datos», de la Circular 1/2013, de 24 de mayo, o cualquier otra referencia o código determinado por la entidad. Pero, en todo caso, se utilizará el mismo código de operación o referencia, para identificar a la misma operación en las diferentes tablas de datos del documento «Información Financiera-PYME»."
        		+"\n(3) Este campo se informará siguiendo los valores establecidos en el campo «Límite máximo a disposición del prestatario al inicio de la operación», del módulo B.2, del anejo 2, «Instrucciones para elaborar los módulos de datos», de la Circular 1/2013, de 24 de mayo.\n(4) Para las operaciones canceladas durante los cinco años de referencia, estos campos se informarán como «n/a» (no aplicable).\n(5) Este campo incluirá tanto el riesgo con disponibilidad inmediata como el riesgo con disponibilidad condicionada."
        		+"\n(6) Para las operaciones canceladas antes de junio de 2015 se indicará «n/a» (no aplicable).\n(7) Se incluirán también el resto de las garantías personales aportadas por el acreditado relacionadas con la operación pero no incluidas en el propio contrato de la operación.\n(8) El campo «Otras observaciones» se utilizará para reflejar aquella información necesaria para el mejor entendimiento de los datos presentados en las tablas.\n(*) Lista de valores en Anejo.\nLos importes figuran en miles de euros.", new Font(BankiaFont.getBaseFont(), 10));
        paragraph.setSpacingBefore(10);
        paragraph.setLeading(10, 0);
        document.add(paragraph);
        // SIXTH PAGE
        document.newPage();
        mmWidths =  new String[] {"24.5f", "24.5f", "24.5f", "24.5f", "24.5f", "24.5f", "24.5f", "24.5f", "24.5f", "24.5f", "24.5f"};
        headers =  new String[] {"comunicaciones.rai.efecto","comunicaciones.rai.situacion", "comunicaciones.rai.tipo", "comunicaciones.rai.vencimiento", "comunicaciones.rai.moneda", "comunicaciones.rai.impagado", "comunicaciones.rai.impagado", "comunicaciones.rai.impagado", "comunicaciones.rai.impagado", "comunicaciones.rai.impagado", "comunicaciones.rai.nominal"};        
        RaiInformation = TableBuilder.getTable(mmWidths, headers, 10);
        RaiInformation.getDefaultCell().setFixedHeight(75.19f);
            // 1st row
            // 2nd and following rows

//            for (int i = 0; i<2; i++) {
                for (int j = 0; j < 11; j++) {
//                	// auxiliary array for simulated data
//                	String[] auxArray = new String[]{"99999999999999999", "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", "XXXXXXXXXXXXXXXXX", "XXXXXXXX", "XXX", "Z.ZZZ.ZZZ.ZZZ.ZZZ,ZZ", "Z.ZZZ.ZZZ.ZZZ.ZZZ,ZZ"};
//                	cell = new PdfPCell(new Phrase(auxArray[j], fontRed9));
//                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                	RaiInformation.addCell(new Phrase("hola"));
//                }
            }
            // set 'row position' of the row that will be married to the precedent row 
            RaiInformation.keepRowsTogether(1552);
        	// add table to the page with chosen space before and after it, and set a possible break point
            RaiInformation.setSpacingBefore(25f);
            RaiInformation.setSpacingAfter(8f);
            if (RaiInformation.getRows().size() > 18) {
                RaiInformation.setBreakPoints(19);
            }  
            document.add(RaiInformation);   
        //END PAGE
        document.newPage();
        paragraph = new Paragraph("6.2.	Posición relativa del acreditado respecto a su sector de actividad.", new Font(BankiaFont.getBaseBoldFont(), 11));
        paragraph.setSpacingBefore(5);
        paragraph.setSpacingAfter(-7);
        document.add(paragraph);
        // build a table with mmWidths and headers data in form of strings
        mmWidths =  new String[] {"25,5", "93", "27.3", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5", "9,5"};
        headers =  null;
        PdfPTable IdentificationForSummary1 = TableBuilder.getTable(mmWidths, headers, 9);
		do {			
			for (int i = 0; i < 9; i++) {
				cell = new PdfPCell(new Phrase(" "));
				// is this one exact size according to the xdoc file???:
				// cell.setFixedHeight(8.22f);
				if (i == 0 && IdentificationForSummary1.getLastCompletedRowIndex() == -1) { 
					cell = new PdfPCell(new Phrase("Empresa", new Font(BankiaFont.getBaseBoldFont(), 9)));
				}
				if (i == 0 && IdentificationForSummary1.getLastCompletedRowIndex() != -1) {
					cell = new PdfPCell(new Phrase("Sector", new Font(BankiaFont.getBaseBoldFont(), 9)));
				}
				if (i == 1) {
					cell = new PdfPCell(new Phrase("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", fontRed10));
					cell.setColspan(2);					
				}if (i == 5 && IdentificationForSummary1.getLastCompletedRowIndex() == -1) { 
					cell = new PdfPCell(new Phrase("NIF", new Font(BankiaFont.getBaseBoldFont(), 9)));
					cell.setColspan(2);
				}
				if (i == 5 && IdentificationForSummary1.getLastCompletedRowIndex() != -1) {
					cell = new PdfPCell(new Phrase("Tamaño", new Font(BankiaFont.getBaseBoldFont(), 9)));
					cell.setColspan(2);
				}				
				if (i == 6 && IdentificationForSummary1.getLastCompletedRowIndex() == -1) { 
					cell = new PdfPCell(new Phrase("XXXXXXXXXXX", fontRed9));
					cell.setColspan(4);
				}
				if (i == 6 && IdentificationForSummary1.getLastCompletedRowIndex() != -1) {
					cell = new PdfPCell(new Phrase("XXXXXXXXXXXXXXXX", fontRed9));
					cell.setColspan(4);
				}
				if (i == 7) cell = new PdfPCell(new Phrase("CNAE", new Font(BankiaFont.getBaseBoldFont(), 9)));
				if (i == 8) {
					cell = new PdfPCell(new Phrase("XXXXX", fontRed9));
					cell.setColspan(2);
				}				
				IdentificationForSummary1.addCell(cell);
			}
			for (int i = 0; i < 14; i++) {
				cell = new PdfPCell(new Phrase(" "));
				// is this one exact size according to the xdoc file???:
				// cell.setFixedHeight(8.22f);
				// System.out.println(IdentificationForSummary1.getLastCompletedRowIndex());
				if (i == 0 && IdentificationForSummary1.getLastCompletedRowIndex() == 0) { 
					cell = new PdfPCell(new Phrase("Denominación social", new Font(BankiaFont.getBaseBoldFont(), 8)));
				}
				if (i == 0 && IdentificationForSummary1.getLastCompletedRowIndex() != 0) {
					cell = new PdfPCell(new Phrase("Agregado", new Font(BankiaFont.getBaseBoldFont(), 9)));
				}
				if (i == 1) {
					cell = new PdfPCell(new Phrase("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", fontRed10));
					cell.setColspan(2);
				}					
				IdentificationForSummary1.addCell(cell);
			}
			for (int i = 0; i < 15; i++) {
				cell = new PdfPCell(new Phrase(" "));
				// exact size according to the xdoc file:
				// cell.setFixedHeight(8.22f);
				cell.setFixedHeight(9f);
				IdentificationForSummary1.addCell(cell);
			}
		} while (IdentificationForSummary1.size() < 6);
		IdentificationForSummary1.setSpacingBefore(20f);
		for (PdfPRow row : (IdentificationForSummary1.getRows())) {
			for (PdfPCell c : row.getCells()) {
				if (c != null) c.setBorder(PdfPCell.NO_BORDER);
			}
		}
		document.add(IdentificationForSummary1);
		// 2nd auxiliary table for the Summary table
		PdfPTable IdentificationForSummary2 = TableBuilder.getTable(mmWidths, headers, 9);
        IdentificationForSummary1.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
		IdentificationForSummary2.addCell(new Phrase("Área de análisis", new Font(BankiaFont.getBaseBoldFont(), 9)));
		IdentificationForSummary2.addCell(new Phrase("Ratio (%)", new Font(BankiaFont.getBaseBoldFont(), 9)));
		IdentificationForSummary2.addCell(new Phrase("Valor en la empresa", new Font(BankiaFont.getBaseBoldFont(), 9)));
		cell = new PdfPCell(new Phrase("Posición en el sector", new Font(BankiaFont.getBaseBoldFont(), 9)));
		cell.setColspan(12);
		IdentificationForSummary2.addCell(cell);
		IdentificationForSummary2.addCell(new Phrase(""));
		IdentificationForSummary2.addCell(new Phrase(""));
        paragraph = new Paragraph("Año:", new Font(BankiaFont.getBaseBoldFont(), 11));
        chunk = new Chunk(" AAAA", fontRed11);
        paragraph.add(chunk);
		IdentificationForSummary2.addCell(paragraph);
		cell = new PdfPCell(paragraph);
		cell.setColspan(12);
		cell.setFixedHeight(15.5f);
		IdentificationForSummary2.addCell(cell);
		for (PdfPRow row : (IdentificationForSummary2.getRows())) {
			for (PdfPCell c : row.getCells()) {
				if (c != null) c.setBorder(PdfPCell.NO_BORDER);
			}
		}
		document.add(IdentificationForSummary2);
		// Summary table
		PdfPTable Summary = TableBuilder.getTable(mmWidths, headers, 9);
		Summary.getDefaultCell().setBorder(PdfPCell.NO_BORDER);        
        try {
        	Summary.getDefaultCell().setBorder(PdfPCell.TOP);
        	Summary.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
        	Summary.addCell(new Phrase("Capital circulante", new Font(BankiaFont.getBaseBoldFont(), 9)));
        	Summary.addCell(new Phrase("R24. Deudas con entidades de crédito / total patrimonio neto y pasivo (*)", new Font(BankiaFont.getBaseFont(), 9)));
        	Summary.addCell(new Phrase("ZZ.ZZZ.ZZZ,ZZ", fontRed9));
        	Summary.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        	Summary.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
        	cell = new PdfPCell();
        	// 1st row
    		// red color
    		Summary.getDefaultCell().setBackgroundColor(new BaseColor(255, 0, 0));
    		Summary.getDefaultCell().setBorder(PdfPCell.RECTANGLE | PdfPCell.TOP);
    		Summary.getDefaultCell().setColspan(3);
    		Summary.addCell("");
    		Summary.getDefaultCell().setColspan(1);        		
        	// orange color
        	Summary.getDefaultCell().setBackgroundColor(new BaseColor(255, 102, 0));
    		Summary.getDefaultCell().setColspan(3);
    		Summary.addCell("");
    		Summary.getDefaultCell().setColspan(1);
        	// green color
    		Summary.getDefaultCell().setBackgroundColor(new BaseColor(119, 238, 0));
    		Summary.getDefaultCell().setColspan(3);
    		Summary.addCell("");
    		Summary.getDefaultCell().setColspan(1);
    		// light green color
    		Summary.getDefaultCell().setBackgroundColor(new BaseColor(71, 175, 91));
    		Summary.getDefaultCell().setColspan(3);
    		Summary.addCell("");
    		Summary.getDefaultCell().setColspan(1);
        	Summary.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
        	// 2nd row
        	Summary.getDefaultCell().setBackgroundColor(new BaseColor(255, 255, 255));
        	for (int j = 0; j < 5; j++) {
        		Summary.addCell("");
        	}
        	cell = new PdfPCell(new Phrase("XXXXXXXXXX", fontRed9));
        	cell.setColspan(2);
        	cell.setFixedHeight(13f);
        	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	cell.setBorder(PdfPCell.RIGHT | PdfPCell.LEFT | PdfPCell.BOTTOM);
        	Summary.addCell(cell);      		
        	Summary.addCell("");
        	Summary.addCell(cell);
        	Summary.addCell("");        	
        	Summary.addCell(cell);
        	Summary.addCell("");
        	Summary.addCell("");
        	// 3rd row
        	for (int i = 0; i < 15; i++) {
        		cell = new PdfPCell(new Phrase(" "));
        		// exact size according to the xdoc file:
        		// cell.setFixedHeight(8.22f);
        		cell.setFixedHeight(9f);
        		cell.setBorder(PdfPCell.NO_BORDER);
        		Summary.addCell(cell);
        	}
//        	Summary.addCell(new Phrase("Capital circulante", new Font(BankiaFont.getBaseBoldFont(), 9)));
//        	Summary.addCell(new Phrase("R24. Deudas con entidades de crédito / total patrimonio neto y pasivo (*)", new Font(BankiaFont.getBaseFont(), 9)));
//        	Summary.addCell(new Phrase("ZZ.ZZZ.ZZZ,ZZ", new Font(BankiaFont.getBaseFont(), 10)));
        	cell = new PdfPCell();
        	do {
				// 4th row
        		Summary.addCell(new Phrase("Capital circulante", new Font(BankiaFont.getBaseBoldFont(), 9)));
            	Summary.addCell(new Phrase("R24. Deudas con entidades de crédito / total patrimonio neto y pasivo (*)", new Font(BankiaFont.getBaseFont(), 9)));
            	Summary.addCell(new Phrase("ZZ.ZZZ.ZZZ,ZZ", fontRed9));
				
            	Summary.getDefaultCell().setBorder(PdfPCell.RECTANGLE | PdfPCell.TOP);
				// red color
				Summary.getDefaultCell().setBackgroundColor(
						new BaseColor(255, 0, 0));
        		Summary.getDefaultCell().setColspan(3);
        		Summary.addCell("");
        		Summary.getDefaultCell().setColspan(1);				
				// orange color
				Summary.getDefaultCell().setBackgroundColor(
						new BaseColor(255, 102, 0));
        		Summary.getDefaultCell().setColspan(3);
        		Summary.addCell("");
        		Summary.getDefaultCell().setColspan(1);
				// green color
				Summary.getDefaultCell().setBackgroundColor(
						new BaseColor(119, 238, 0));
        		Summary.getDefaultCell().setColspan(3);
        		Summary.addCell("");
        		Summary.getDefaultCell().setColspan(1);
				// light green color
				Summary.getDefaultCell().setBackgroundColor(
						new BaseColor(71, 175, 91));
        		Summary.getDefaultCell().setColspan(3);
        		Summary.addCell("");
        		Summary.getDefaultCell().setColspan(1);
				Summary.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				// 5th row
				Summary.getDefaultCell().setBackgroundColor(
						new BaseColor(255, 255, 255));
				for (int j = 0; j < 5; j++) {
					Summary.addCell("");
				}
				cell = new PdfPCell(new Phrase("XXXXXXXXXX", fontRed9));
				cell.setColspan(2);
				cell.setFixedHeight(13f);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				Summary.addCell(cell);
				Summary.addCell("");
				Summary.addCell(cell);
				Summary.addCell("");
				Summary.addCell(cell);
				Summary.addCell("");
				Summary.addCell("");
				// 6th row
				PdfPCell[] row = Summary.getRow(2).getCells();
				for (int i = 0; i < row.length; i++) {
					cell = row[i]; 
					cell.setFixedHeight(9f);
					Summary.addCell(cell);
				}
			} while (Summary.size() < 27);
            // set 'row position' of the row that will be married to the precedent row 
            Summary.keepRowsTogether(1552);
        	// add table to the page with chosen space before and after it, and set a possible break point
            if (Summary.getRows().size() > 18) {
            	Summary.setBreakPoints(19);
            }  
            document.add(Summary);
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
        Image img = Image.getInstance(CreatePdf.class
				.getClassLoader().getResource("cellSymbol.png"));
        img.scaleAbsolute(16.5f, 16.5f);
        // img.setAbsolutePosition(474,397.28f);
        float leftLimit = 457f;
        float rightLimit = 761f;
        float y = 370.5f;
        img.setAbsolutePosition(457f,y);
        writer.getDirectContent().addImage(img);
        float distance = 34.65f;
        for (int i = 0; i < 8; i++) {
        	float x = leftLimit + new Random().nextFloat() * (rightLimit - leftLimit);
            img.setAbsolutePosition(x,y-distance);
            writer.getDirectContent().addImage(img);
            distance = distance + 34.89f;
		}

        // finally, close the document
        document.close();

        // try to launch the associated application registered on system to open the generated pdf file
        try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}