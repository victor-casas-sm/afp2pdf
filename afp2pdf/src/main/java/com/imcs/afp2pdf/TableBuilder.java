package com.imcs.afp2pdf;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.PdfPTable;

class TableBuilder {
	
	static float[] widths;
	static float sumWidths;
	static Properties properties = new Properties();
	


	static PdfPTable getTable(String[] mmWidths, String[] headers, float size) throws IOException {
		widths = new float[mmWidths.length];
		sumWidths=0;
		for (int i = 0; i < mmWidths.length; i++) {			
			mmWidths[i] = mmWidths[i].replace(',', '.');
			widths[i] = Utilities.millimetersToPoints(Float.parseFloat(mmWidths[i]));
			sumWidths = sumWidths + widths[i];
 		}		
		PdfPTable pdfPTable = new PdfPTable(widths);
        pdfPTable.setTotalWidth(sumWidths);
        pdfPTable.setLockedWidth(true);
        pdfPTable.setHorizontalAlignment(0);
        InputStream in = CreatePdf.class.getClassLoader().getResourceAsStream("app.properties");
        properties.load(in);
        in.close();
        try {           
            //1st row is set as table header in this implementation of Table Builder
            //TODO arreglar esto de headers null
        	if (headers != null) {
        	pdfPTable.setHeaderRows(1);
            pdfPTable.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            pdfPTable.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            pdfPTable.getDefaultCell().setBackgroundColor(BaseColor.GRAY);
            pdfPTable.getDefaultCell().setFixedHeight(31);
            	for(int i = 0; i < headers.length; i++) {
                    	headers[i] = properties.getProperty(headers[i]);
                	pdfPTable.addCell(new Phrase(headers[i], new Font(BankiaFont.getBaseFont(), size)));
                }
        	}
        } catch(DocumentException de) {
            throw new ExceptionConverter(de);
        }
		return pdfPTable;
	}

}
